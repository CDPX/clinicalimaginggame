﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;




public class GetIDList : MonoBehaviour {

	public GameObject easy;
	public GameObject med;
	public GameObject hard;

	public GameObject textInput;

	int numberOfLevels = 10;

	int difficulty = 2;

	void Update() {
		//print(difficulty);
	}

	public void difficultyChosen(int i) {
		difficulty = i + 1;	

	//Insert Selection UI here to show button selected


	}

	public void next() {
		StartCoroutine(getids());
	}

	IEnumerator getids() {

		string num = textInput.GetComponent<Text>().text;
	

		string url = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/getids.php";

		WWWForm form = new WWWForm();
        form.AddField("difficulty", difficulty);
        form.AddField("number", num);
        WWW formLink = new WWW(url, form);

        yield return formLink;

        List<int> ids = new List<int>();

        string[] nums = formLink.text.Split(',');

        for (int i = 0; i < nums.Length - 1; i++) {
        	ids.Add(int.Parse(nums[i]));
        }

        Classroom.ids = ids;

        Classroom.toImages();
    


	}
}
