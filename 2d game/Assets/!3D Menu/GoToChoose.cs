﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoToChoose : MonoBehaviour {

    public Button Button;



    void Start()
    {

        Button btn1 = Button.GetComponent<Button>();

        btn1.onClick.AddListener(change);
    }

    private void change()
    {
        SceneManager.LoadScene("Choose profile");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
