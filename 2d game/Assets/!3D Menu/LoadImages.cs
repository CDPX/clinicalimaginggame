﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class LoadImages : MonoBehaviour {

	public List<Texture2D> xrays = new List<Texture2D>();
	public List<Vector2> cos = new List<Vector2>();
	Texture2D img;
	string image = "";

	// Use this for initialization
	void Start () {
		StartCoroutine(Loadimg());
	}

	IEnumerator Loadimg() {

		
		print("Started");
		

		string url = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/index.php";
		WWWForm imageForm = new WWWForm();

		List<int> current_ids = new List<int>() {
			18, 60 ,61, 62, 65,66,67,68, 70, 75
		};

		GameMan.ids = current_ids;


		for(int i=0; i < current_ids.Count; i++){


	        print(current_ids[i]);
	        imageForm.AddField("Clinical_imagesPost", current_ids[i]);
	        //imageForm.AddField("Clinical_levelPost", imageID[ID]);
	        WWW imgLink = new WWW(url, imageForm);
	        yield return imgLink;
	        image = (imgLink.text);

	     
	       	string[] elements = image.Split('|');


	       	byte[] Bytes = System.Convert.FromBase64String(elements[0]);


	       	float xBef = (float)Int32.Parse(elements[1]);
	       	float yBef = (float)Int32.Parse(elements[2]);
	       	
	       	float newX = (float)((xBef/2048)*20);
	       	float newY = (float)((yBef/2048)*20);


			print("$$");
			print(elements[0]);
			print(elements[1]);
			print(elements[2]);

			newX = newX - 10;
			newY = 10 - newY;

			print("X: " + newX);
	       	print("Y: " + newY);

	       	print("$$");
	        
	        Texture2D texture = new Texture2D(1, 1);
	        texture.LoadImage(Bytes);
	        xrays.Add(texture);
	        cos.Add(new Vector2(newX,newY));

	        setXrays();

       
       }

       

       print(GameMan.xrays[0]);

	}


	public void setXrays() {
		GameMan.xrays = xrays;
		GameMan.cos = cos;
	}
	
	
}
