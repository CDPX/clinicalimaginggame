﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoginTo3D : MonoBehaviour {

    public Button Button;
    // Use this for initialization
    void Start()
    {

        Button btn1 = Button.GetComponent<Button>();

        btn1.onClick.AddListener(change);
    }

    private void change()
    {
        SceneManager.LoadScene("Clinic");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
