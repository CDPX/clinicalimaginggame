﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SignUpToLogin : MonoBehaviour
{

    public Button Button1;
    // Use this for initialization
    void Start()
    {

        Button btn1 = Button1.GetComponent<Button>();

        btn1.onClick.AddListener(Change);
    }

    private void Change()
    {
        SceneManager.LoadScene("login");
    }

    // Update is called once per frame
    void Update()
    {

    }
}