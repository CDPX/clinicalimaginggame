﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gotofinalise : MonoBehaviour
{

    // Use this for initialization
    public Button Button;



    void Start()
    {

        Button btn1 = Button.GetComponent<Button>();

        btn1.onClick.AddListener(change);
    }

    private void change()
    {
        SceneManager.LoadScene("finalise image");
    }

    // Update is called once per frame
    void Update()
    {

    }
}