﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menu : MonoBehaviour {


    public GameObject btnObj;
    public GameObject caidan;
    public Sprite expan;
    public Sprite back;
    Button btn;
    bool isshow = false;
    // Use this for initialization
    void Start()
    {
        caidan.SetActive(isshow);
        btn = btnObj.GetComponent<Button>();
        btn.onClick.AddListener(delegate ()
        {
            isshow = !isshow;
            caidan.SetActive(isshow);
            if (isshow)
            {
                btn.GetComponent<Image>().sprite = expan;
            }
            else
            {
                btn.GetComponent<Image>().sprite = back;
            }
        });
    }

	
	// Update is called once per frame
	void Update () {
		
	}
}
