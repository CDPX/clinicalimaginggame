﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoosingGame : MonoBehaviour 
{
    public GameObject window;
    public GameObject LevelOne;
    public GameObject LevelTwo;
    public GameObject LevelThree;
    public GameObject LevelFour;
    public GameObject LevelFive;
    public Toggle EasyToggle;
    public Toggle MediumToggle;
    public Toggle HardToggle;

    private int total;
    private int levelOne;
    private int levelTwo;
    private int levelThree;
    private int levelFour;
    private int levelFive;
    private int levelTotal;
    public Text messageField;

    string sendurl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/sendData.php";
    // Use this for initialization
    public void Show(string message)
    {
        if(levelTotal != 0)
        {
            levelTotal = 0;
        } 

        total = 10; 
        if (levelOne < 0 || levelTwo < 0 || levelThree < 0 || levelFour < 0 || levelFive < 0)
        {
            messageField.text = "The mini games level entered cannot be a negative number.";
            window.SetActive(true);
            return;
        }
        levelTotal = levelOne + levelTwo + levelThree + levelFour + levelFive;
        if ((EasyToggle.isOn || MediumToggle.isOn || HardToggle.isOn) && levelTotal != 0)
        {
            messageField.text = "you can only either selected an automated difficulty or customise your level.";
            window.SetActive(true);
            return;
        }
        else if ((EasyToggle.isOn || MediumToggle.isOn || HardToggle.isOn) && levelTotal == 0)
        {
            if (EasyToggle.isOn)
            {
                levelOne = total;
            }
            else if (MediumToggle.isOn)
            {
                levelTwo = total / 2;
                levelThree = total - levelTwo;
            }
            else if (HardToggle.isOn)
            {
                levelFour = total / 2;
                levelFive = total - levelFour;
            }
        }
        else if ((!EasyToggle.isOn && !MediumToggle.isOn && !HardToggle.isOn))
        {
            if (levelTotal != total)
            {
                messageField.text = "The sum of each mini game levels you've selected needs to be equal to the total";
                window.SetActive(true);
                return;
            }
            else if (levelTotal == 0)
            {
                messageField.text = "you must either selected an automated difficulty or customise your level.";
                window.SetActive(true);
                return;
            }

        }
        addCode(total, levelOne, levelTwo, levelThree, levelFour, levelFive);

    }

    public void Hide()
    {
        window.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (LevelOne.GetComponent<InputField>().isFocused)
            {
                LevelTwo.GetComponent<InputField>().Select();
            }
            if (LevelTwo.GetComponent<InputField>().isFocused)
            {
                LevelThree.GetComponent<InputField>().Select();
            }
            if (LevelThree.GetComponent<InputField>().isFocused)
            {
                LevelFour.GetComponent<InputField>().Select();
            }
            if (LevelFour.GetComponent<InputField>().isFocused)
            {
                LevelFive.GetComponent<InputField>().Select();
            }
        }

        //total = Total.GetComponent<InputField>().text;
        int i;
        if (int.TryParse(LevelOne.GetComponent<InputField>().text, out i))
        {
            levelOne = i;
        }
        if (int.TryParse(LevelTwo.GetComponent<InputField>().text, out i))
        {
            levelTwo = i;
        }
        if (int.TryParse(LevelThree.GetComponent<InputField>().text, out i))
        {
            levelThree = i;
        }
        if (int.TryParse(LevelFour.GetComponent<InputField>().text, out i))
        {
            levelFour = i;
        }
        if (int.TryParse(LevelFive.GetComponent<InputField>().text, out i))
        {
            levelFive = i;
        }
    }

    public void addCode(int total, int levelOne, int levelTwo, int levelThree, int levelFour, int levelFive)
    {
        //Form enables you to send data to php file
        WWWForm codeForm = new WWWForm();
        print("working");
        //each field represents a post request 
        // statsForm.AddField 
        //("POST NAME according to php file", corresponding value you want to send)
        codeForm.AddField("total_Post", total);
        codeForm.AddField("lvlOne_Post", levelOne);
        codeForm.AddField("lvlTwo_Post", levelTwo);
        codeForm.AddField("lvlThree_Post", levelThree);
        codeForm.AddField("lvlFour_Post", levelFour);
        codeForm.AddField("lvlFive_Post", levelFive);

        //creates the connection and sends all the data 
        WWW codeWWW = new WWW(sendurl, codeForm);
    }
	
}
