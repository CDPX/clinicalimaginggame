﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CodeGenerator : MonoBehaviour
{
    //store all the code with a unique id
    private string characters = "0123456789abcdefghijklmnopqrstuvwxABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private int codeLength = 5;
    string sendurl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/sendCode.php";

    public GameObject TextBox;
    public string TheCharacter;

    public void Start() {
        print("Heloo");
        randomGenerator();
    }

    public void randomGenerator()
    {
        //erase the previous code
        if (TheCharacter != "")
        {
            //TODO delete code from database
            TheCharacter = "";
        }
        while (TheCharacter.Length < codeLength)
        {
            TheCharacter += characters[Random.Range(0, characters.Length)];
        }

        Classroom.code = TheCharacter.ToString();

        print(Classroom.code);
        
        addCode(TheCharacter.ToString());
    }



    public void addCode(string code)
    {
        //Form enables you to send data to php file
        WWWForm codeForm = new WWWForm();

        //each field represents a post request 
        // statsForm.AddField 
        //("POST NAME according to php file", corresponding value you want to send)
        codeForm.AddField("code_Post", code);

        //creates the connection and sends all the data 
        WWW codeWWW = new WWW(sendurl, codeForm);
    }

}
