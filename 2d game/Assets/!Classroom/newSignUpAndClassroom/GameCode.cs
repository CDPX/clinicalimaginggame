﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class GameCode : MonoBehaviour 
{
    public GameObject inputCode;
    public GameObject window;

    private string code;
    private int count = 1;
    private string[] databaseCode = new string[1000];
    private string message;

    public Text messageField;
    string geturl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/getCode.php";

    public void show(string message)
    {
        messageField.text = message;
        window.SetActive(true);
    }

    public void hide()
    {
        window.SetActive(false);
    }

    public void verifyGameCode()
    {
        code = inputCode.GetComponent<InputField>().text;
        for (int i = 0; i < databaseCode.Length; i++)
        {
            print(databaseCode[i]);
            message = "";
            if(code == databaseCode[i])
            {
                message = "You will be directed to the game.";
                show(message);
                return;
                //link to the game
            }
            else if(i == databaseCode.Length - 1 && code != databaseCode[i])
            {
                message = "Incorrect code";
                show(message);
                code = inputCode.GetComponent<InputField>().text;
                //print(code);
            }
        }
    }

    IEnumerator Start()
    {
        //to get Stats
        WWWForm codeForm = new WWWForm();
        //change number 1 to a variable which will store the code id
        codeForm.AddField("code_Post", count);
        WWW codeLink = new WWW(geturl, codeForm);
        yield return codeLink;
        while(count < 1000)
        {
            databaseCode[count - 1] = codeLink.text;
            count++;
            codeForm.AddField("code_Post", count);
            //print(databaseCode);
            codeLink = new WWW(geturl, codeForm);
            yield return codeLink;
        }
    }
   
}
