﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetImages : MonoBehaviour {

	string url = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/otherindex.php";
	string updateurl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/updateId.php";

	public GameObject panel;
	public GameObject block;

	public static List<int> class_xrays = new List<int>();

	IEnumerator Start() {

		//Used for testing
		List<int> current_ids = new List<int>() {
			18, 60 ,61, 62, 65,66,67,68, 70, 75
		};

		current_ids = Classroom.ids;


		for(int i=0; i < current_ids.Count; i++){

			WWWForm form = new WWWForm();

	        form.AddField("Clinical_imagesPost", current_ids[i]);
	        WWW formLink = new WWW(url, form);
	        yield return formLink;

	        print(formLink.text);

	        string[] words = formLink.text.Split('|');

	        var newBlock = Instantiate(block, panel.transform);

	        class_xrays.Add(int.Parse(words[0]));

	        foreach (Transform child in newBlock.transform) {
	             if (child.tag == "pic") {
	                byte[] Bytes = System.Convert.FromBase64String(words[1]);
	                Texture2D texture = new Texture2D(1, 1);
	        		texture.LoadImage(Bytes);
	        		Sprite xPic = Sprite.Create(texture, new Rect(0,0,texture.width, texture.height), new Vector2(0f, 0f), 100.0f);
	        		child.GetComponent<Image>().sprite = xPic;
	            }
	        }

	    }
	}

	void change(int i) {
		//Get id of image changing
		//Update in class_xrays block
	}

	public void save() {
		StartCoroutine(saveEverything());
	}

	IEnumerator saveEverything() {

		string all = "";

		for(int i=0; i < class_xrays.Count; i++){

			all += class_xrays[i].ToString() + ",";

		}

		WWWForm form = new WWWForm();
		Debug.Log("HERE");
        form.AddField("xray_id'sPost", all);
        WWW formLink = new WWW(updateurl, form);

		yield return "hello";

		Classroom.toCode();

		
		
        

	}
	
	
}
