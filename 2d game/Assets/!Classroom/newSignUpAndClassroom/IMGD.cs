﻿//using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class IMGD : MonoBehaviour 
{
    public GameObject window;
    public Text messageField;

    string getImgUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/otherindex.php";
    string getTotalUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/totalImg.php";
    string getLevelUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/getLevel.php";
    string sendUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/updateId.php";

    private string[,] databaseCode;
    private string[] databaseImg;
    private string databaseLvl;
    private string[,] images;
    private string[] topicArray = new string[] { "Total:", "One:", "Two:", "Three:", "Four:", "Five:"};

    private string[] databaseLevel;
    private int total;
    private int i;
    private int count = 0;
    private int levelTotal;
    private int crr;
    private int y;
    private int locate;

    Texture2D img;
    Texture2D texture;
    Texture2D[] imgs;

    public void verifyGameCode()
    {
        locate = 0;
        string temp;
        int tmp = 0;
        string[,] array = new string[databaseCode.Length / 3, 2];
        if (int.TryParse(EventSystem.current.currentSelectedGameObject.name, out i))
        {
            //print("work");
            crr = i;
        }
        if (int.TryParse(databaseLevel[0], out i))
        {
            //print("work");
            tmp = i;
        }
        for (int j = 0; j < databaseLevel.Length; j++)
        {
            if(crr > tmp)
            {
                //print("testing");
                if((j+1) < databaseLevel.Length)
                {
                    //print("test");
                    if (int.TryParse(databaseLevel[j+1], out i))
                    {
                        //print(j);
                        tmp += i;
                    }
                }
            }
            else
            {
                y = 0;
                //print(databaseCode.Length / 2);

                for (int a = 0; a < databaseCode.Length / 3; a++)
                {
                    if(databaseCode[a, 2] == (j+1).ToString())
                    {
                        //array[y] = databaseCode[a, 0];
                        y++;
                    }
                }
                array = new string[y, 2];
                int k = 0;
                for (int a = 0; a < databaseCode.Length / 3; a++)
                {
                    //print("------------------------------------------");
                    if (databaseCode[a, 2] == (j + 1).ToString())
                    {
                        array[k, 0] = databaseCode[a, 0];
                        array[k,1] = databaseCode[a, 1];
                        //print(array[k]);
                        k++;
                    }
                }
                //print(array.Length);
                tmp = 0;
                break;
            }
        }
        locate = Random.Range(0, array.Length / 2);
        temp = array[locate, 1];

        //print(databaseCode.Length);
        while (temp == null)
        {
            locate = Random.Range(0, array.Length / 2);
            temp = array[locate, 1];
        }
        //print(temp);
        for (int j = 0; j < images.Length/2; j++)
        {
            //print(j);
            if (images[j, 1] == null)
            {
                //print("null placed");
                images[j, 0] = array[locate, 0];
                images[j, 1] = temp;
                break;
            }
            else
            {
                while(images[j, 1] == temp && array.Length / 2 != 1)
                {
                    
                    locate = Random.Range(0, array.Length / 2);
                    temp = array[locate, 1];
                    j = 0;
                }
                /*else if(images[j] == temp && array.Length == 1)
                {
                    messageField.text = "There's no other minigames to change to";
                    window.SetActive(true);
                    return;
                }*/
            }
        }
        images[crr - 1, 0] = array[locate, 0];
        images[crr - 1, 1] = temp;
        byte[] Bytes = System.Convert.FromBase64String(temp);
        texture = new Texture2D(1, 1);
        texture.LoadImage(Bytes);
        imgs[crr - 1] = texture;
        string result1 = ConvertStringArrayToString(images);
        print(result1);
        /*for (int i = 0; i < 10; i++)
        {
            print(images[i, 0]);
        }*/
        //print(array[locate, 0]);
       
    }

    IEnumerator Start()
    {
       // button1 = GetComponent<Button>();
        //to get Stats
        WWWForm totalForm = new WWWForm();
        totalForm.AddField("Clinical_totalPost", count);
        WWW totalLink = new WWW(getTotalUrl, totalForm);
        yield return totalLink;
        if (int.TryParse(totalLink.text, out i))
        {
            //print("work");
            total = i;
        }
        //print(total);

        databaseCode = new string[total, 3];
        databaseImg = new string[total];
        databaseLevel = new string[5];

        //getting the levels set from choosing games
        WWWForm codeForm = new WWWForm();
        WWWForm levelForm = new WWWForm();
        levelForm.AddField("Clinical_levelPost", count);
        WWW levelLink = new WWW(getLevelUrl, levelForm);
        yield return levelLink;
        databaseLvl = levelLink.text;
        for (int j = 0; j < 6; j++)
        {
            if (j == 0)
            {
                if (int.TryParse(getValue(databaseLvl, topicArray[j], "|"), out i))
                {
                    //print("work");
                    levelTotal = i;
                }
            }
            else
            {
                databaseLevel[j - 1] = getValue(databaseLvl, topicArray[j], "|");

            }
        }

        imgs = new Texture2D[levelTotal];
        images = new string[levelTotal, 2];
        //print(databaseLevel[1]);
        //change number 1 to a variable which will store the code id
        //getting the images and the level of difficulty of each images
        while (count < total)
        {
            count++;

            codeForm.AddField("Clinical_imagesPost", count);
            WWW codeLink = new WWW(getImgUrl, codeForm);
            yield return codeLink;
            //print(codeLink.text);
            //change the while loop
            databaseImg[count] = codeLink.text;
            //print(databaseImg[count - 1]);
            print(count);
            databaseCode[count, 0] = (getValue(databaseImg[count], "ID:", "|"));
            databaseCode[count, 1] = (getValue(databaseImg[count], "Image:", "|"));
            //print(databaseCode[count - 1, 0]);
            databaseCode[count, 2] = (getValue(databaseImg[count], "Level:", ";"));
            //print(databaseCode[count, 1]);

            if (count == 6) {
                break;
            }
            
        }
        int x = 0; 
        for (int z = 0; z < levelTotal; z++)
        {
            int crr = 0;
            if (int.TryParse(databaseLevel[x], out i))
            {
                //print(i);
                crr = i;
            }
            y = 0;
            //print(levelTotal);
            //string[] tmp = new string[databaseCode.Length / 2];
            for (int k = 0; k < databaseCode.Length / 3; k++)
            {
                
                if(databaseCode[k, 2] == (x + 1).ToString())
                {
                    //print(databaseCode[k, 1]);
                    //tmp[y] = databaseCode[k, 0];
                    y++;
                    //print(tmp[y]);
                }
            }

            string[,] tmp = new string[y, 2];
            y = 0; 
            for (int k = 0; k < databaseCode.Length / 3; k++)
            {

                if (databaseCode[k, 2] == (x + 1).ToString())
                {
                    tmp[y, 0] = databaseCode[k, 0];
                    tmp[y, 1] = databaseCode[k, 1];
                    y++;
                    //print(tmp[y]);
                }
            }
            //print("database");
            string temp;
                    
            locate = Random.Range(0, tmp.Length / 2);
            print(locate);
            temp = tmp[locate, 1];
            while(temp == null)
            {
                locate = Random.Range(0, tmp.Length / 2);
                temp = tmp[locate, 1];
            }
            //print(temp);
            for (int j = 0; j < levelTotal; j++)
            {
                //print(j);
                if (images[j, 1] == null)
                {
                    //print("null placed");
                    images[j, 0] = tmp[locate, 0];
                    images[j, 1] = temp;
                    break;
                }
                else
                {
                    while(images[j, 1] == temp)
                    {
                        //print("work");
                        locate = Random.Range(0, tmp.Length / 2);
                        temp = tmp[locate, 1];
                        while(temp == null)
                        {
                            locate = Random.Range(0, tmp.Length / 2);
                            temp = tmp[locate, 1];
                        }
                        j = 0; 
                    }
                }
            }
            //print(z);
            //images[z, 0] = tmp[locate, 0];
            //images[z] = temp;
            byte[] Bytes = System.Convert.FromBase64String(temp);
            texture = new Texture2D(1, 1);
            texture.LoadImage(Bytes);
            imgs[z] = texture;
            //print("working");

            if(z > crr - 2 && x < databaseLevel.Length)
            {
                x++;   
            }
        }
        string result1 = ConvertStringArrayToString(images);
        addIds(result1);
        print(result1);
        /*for (int i = 0; i < 10; i++)
        {
            print(images[i, 0]);
        }*/
    }

   /* public void Hide()
    {
        window.SetActive(false);
    }*/

    static string ConvertStringArrayToString(string[,] array)
    {
        // Concatenate all the elements into a StringBuilder.
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 10; i++)
        {
            builder.Append(array[i,0]);
            //builder.Append('.');
        }
        return builder.ToString();
    }

    //seperate the components from php
    string getValue(string data, string level, string character)
    {
        string value = data.Substring(data.IndexOf(level) + level.Length);
        if (value.Contains(character))
        {
            value = value.Remove(value.IndexOf(character));
        }
        return value;
    }

    public void addIds(string ids)
    {
        //Form enables you to send data to php file
        WWWForm codeForm = new WWWForm();

        //each field represents a post request 
        // statsForm.AddField 
        //("POST NAME according to php file", corresponding value you want to send)
        codeForm.AddField("xray_id'sPost", ids);

        //creates the connection and sends all the data 
        WWW codeWWW = new WWW(sendUrl, codeForm);
    }

    void OnGUI()
    {
        int j = 0;
        if(imgs != null)
        {
            for (int i = 0; i < imgs.Length; i++)
            {
                if(i < 5)
                {
                    GUI.Box(new Rect((i + 1) * 50 + (i * 80), 100, 80, 80), imgs[i]);
                }
                else
                {
                    GUI.Box(new Rect((j + 1) * 50 + (j * 80), 300, 80, 80), imgs[i]);
                    j++;
                }


            }
        }
    }
}

/*public static class ArrayExtensions
{
    // This is an extension method. RandomItem() will now exist on all arrays.
    public static T RandomItem<T>(this T[] array)
    {
        //print(Random.Range(0, array.Length - 1));
        return array[Random.Range(0, array.Length - 1)];
    }
}*/
