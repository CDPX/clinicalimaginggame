﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System;

public class StudentClassRoom : MonoBehaviour {

	public GameObject can;

	List<int> class_xrays = new List<int>();

	string url = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/getClassroom.php";
	string index = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/index.php";

	public GameObject textInput;


	public void launch() {

		string code = textInput.GetComponent<Text>().text;

		StartCoroutine(load(code));
	}

	IEnumerator load(string code) {

		can.SetActive(false);

		WWWForm form = new WWWForm();

        form.AddField("id", code);
        WWW formLink = new WWW(url, form);
        yield return formLink;
        
        string[] words = formLink.text.Split(',');

        for (int i = 0; i < words.Length - 1; i++) {
        	print(words[i]);
        	class_xrays.Add(int.Parse(words[i]));
        }

        for (int i = 0; i < class_xrays.Count; i++) {
        	int id = class_xrays[i];

        	WWWForm imageForm = new WWWForm();
        	imageForm.AddField("Clinical_imagesPost", id);

        	WWW imgLink = new WWW(index, imageForm);
	        yield return imgLink;
	        string image = (imgLink.text);

	        string[] elements = image.Split('|');


	       	byte[] Bytes = System.Convert.FromBase64String(elements[0]);


	       	float xBef = (float)Int32.Parse(elements[1]);
	       	float yBef = (float)Int32.Parse(elements[2]);
	       	
	       	float newX = (float)((xBef/2048)*20);
	       	float newY = (float)((yBef/2048)*20);


			newX = newX - 10;
			newY = 10 - newY;

	        
	        Texture2D texture = new Texture2D(1, 1);
	        texture.LoadImage(Bytes);
	        GameMan.xrays.Add(texture);
	        GameMan.cos.Add(new Vector2(newX,newY));

        }

        GameMan.nextLevel();

        


		
        

	}
}
