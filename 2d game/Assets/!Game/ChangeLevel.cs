﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLevel : MonoBehaviour {

	public Text text;

	// Use this for initialization
	void Start () {
		int level = GameMan.level;

		text.text = "LEVEL " + level.ToString();
		
	}
	
	// Update is called once per frame
	void Update () {



	}
}
