﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {


	//Variables
	public Texture2D xray;
	public Vector2 cos;
	public Vector3 abnormalityValue = new Vector3();
	public float totalScore;
	public float seconds = 0;
	float resultingScore = 0;
	

	//Animation variables
	bool animatingToNewLevel = false;
	bool animatingToResult = false;
	public GameObject LevelChanger;

	//GameObjects
	public GameObject tag;
	Camera camera;
	public Text score;
	public Text totalScoreText;
	public Image selectedSpot;
	System.String scoreText;
	public GameObject scoreView;
	public GameObject green;
	public GameObject redsquare;

	public Slider confidenceLevel;
	//Need to assign still


	Sprite xPic;


	void Start () {

		fitCameraHeight();
		camera = GameObject.Find("Minmap").GetComponent<Camera>();

		// Retrieve from GameManager
		xray = GameMan.getXray();
		cos = GameMan.getCos();
		abnormalityValue = new Vector3(cos[0],cos[1],0f);
		totalScoreText.GetComponent<Text>().text = GameMan.totalScore.ToString();

		//Place xray on canvas
		xPic = Sprite.Create(xray, new Rect(0,0,xray.width, xray.height), new Vector2(0.5f, 0.5f), 100.0f);
		this.GetComponent<SpriteRenderer>().sprite = xPic;

	}
	
	void Update () {

		seconds += Time.deltaTime;
		
		if (this.GetComponent<SpriteRenderer>().sprite == null) {
			this.GetComponent<SpriteRenderer>().sprite = xPic;
		}

		// Get Mouse Position
		Ray ray = camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		Physics.Raycast(ray, out hit);
		Vector3 guess = hit.point;
		tagAnimation(ray,hit,guess);

		//User clicks
		if(Input.GetMouseButtonDown(0)) {

			if (MouseControls.selectingSpot) {
				selectsSpot(guess);	
			}
		}

		if (animatingToResult) {
			animateToResult();	
		} 

		if (animatingToNewLevel) {
			LevelChanger.GetComponent<LevelChanger>().FadeToLevel();
		}
	}


	public void tagAnimation(Ray ray, RaycastHit hit, Vector3 guess) {

		if (!Physics.Raycast(ray, out hit)) {
			tag.transform.localPosition = new Vector3(11f,tag.transform.localPosition.y,tag.transform.localPosition.z);
		} else if ((hit.collider.gameObject.name == "Tag") && (MouseControls.shootMouse)) {
			print("tag");
			tag.transform.localPosition = new Vector3(13f,tag.transform.localPosition.y,tag.transform.localPosition.z);

			if(Input.GetMouseButtonDown(0)) {
	
				selectsSpot(guess);	
				
			}

		} else {
			tag.transform.localPosition = new Vector3(11f,tag.transform.localPosition.y,tag.transform.localPosition.z);
		}
	}

	public void nextLevel() {
		LevelChanger.GetComponent<LevelChanger>().FadeToLevel();
	}

	

	public void selectsSpot(Vector3 guess) {

		if (MouseControls.canMove) {

			//Places green square on clicked GLOBAL POSITION
			selectedSpot.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y - 15, Input.mousePosition.z);

			//Places red square on same position ON XRAY
			redsquare.transform.position = new Vector3(guess.x, guess.y, 0f);
			redsquare.transform.localPosition = new Vector3(redsquare.transform.localPosition.x, redsquare.transform.localPosition.y, 10f);
		} 
		
		MouseControls.canMove = false;
	}

	public void lockInConfidence() {

		// Setup result canvas
		scoreView.GetComponent<Canvas>().enabled = true;
		redsquare.GetComponent<SpriteRenderer>().enabled = true;
		green.transform.localPosition = new Vector3(abnormalityValue.x, abnormalityValue.y, -0.05f);

		selectedSpot.transform.position = new Vector3(10000f,10000f,10000f);

		// Set up camera for scaling
		camera.orthographicSize = (float)Math.Round(camera.orthographicSize,0);
		animatingToResult = true;

		calculateScore();
	}

	public void calculateScore() {

		float dist = Vector3.Distance(green.transform.position, new Vector3(redsquare.transform.position.x, redsquare.transform.position.y, green.transform.position.z));
		float distance = (int)Math.Round(dist,0);
		float scoreWithoutConfidence;

		
		int [] scoreList = new int[] { 100,90,80,70,60,50,40,30,20,10,0};
	
		scoreWithoutConfidence = scoreList[(int)distance];

	

		float con = confidenceLevel.value;

		scoreText = scoreWithoutConfidence.ToString("R");
		System.String confidenceText = con.ToString("R");

		int tp = 0;
		int fn = 0;

		//Still need to define the 4 categories correctly

		if (scoreWithoutConfidence > 70) {
			tp = 1;
			resultingScore = scoreWithoutConfidence * con;
			score.GetComponent<Text>().text = resultingScore.ToString("N0");
		} else {
			fn = 3;
			resultingScore = scoreWithoutConfidence/con;
			score.GetComponent<Text>().text = resultingScore.ToString("N0");
		}

		totalScore += resultingScore;

		int lvl = GameMan.level - 1;

		

		sendstats.addStats(GameMan.ids[lvl],1,1,(int)con,tp,0,fn,0,seconds);
		GameMan.totalScore += (int)resultingScore;

	}

	public void animateToResult() {

		// Scale the xray
		Vector3 final = new Vector3(0.35f,0.35f,transform.localScale.z);
		transform.localScale = Vector3.Lerp(transform.localScale, final, 2 * Time.deltaTime);

		if ((transform.localScale.x > 0.34f) && (transform.localScale.x  < 0.36f)) {
			score.GetComponent<Text>().enabled = true;
			green.GetComponent<SpriteRenderer>().enabled = true;

	        animatingToResult = false;       
		}

		// Camera returns to starting zoom level and position
		Vector3 zero = new Vector3(0f,0f,0f);
		camera.transform.position = Vector3.Lerp(camera.transform.position, zero, 2 * Time.deltaTime);

		if (camera.orthographicSize < 7) {
			camera.orthographicSize += 0.2f;
		} else if (camera.orthographicSize > 7) {
			camera.orthographicSize -= 0.2f;
		} 

		if ((camera.orthographicSize < 7.2) && (camera.orthographicSize > 6.8)) {
			camera.orthographicSize = 7;			
		}
	}

	


	void fitCameraHeight() {

	    double height = 15f;
	    double worldScreenHeight = Camera.main.orthographicSize * 2.0;
	    transform.localScale = new Vector2 (1, 1) * (float)(worldScreenHeight / height);
	}



}
