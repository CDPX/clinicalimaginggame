﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMan {

	public GameObject getImages;
	public static List<Texture2D> xrays = new List<Texture2D>();
	public static List<Vector2> cos = new List<Vector2>();
	public static List<int> ids = new List<int>();

	public static int level = 0;
	public static int totalScore;


	// Use this for initialization
	[RuntimeInitializeOnLoadMethod]
	static void OnRuntimeMethodLoad() {
		

	}


	public static void nextLevel() {


		level++;

		if (level == xrays.Count) {
			SceneManager.LoadScene("Clinic");
		}

		SceneManager.LoadScene("Level Transition Scene", LoadSceneMode.Additive);

		if (level == 1) {
			
			//SceneManager.UnloadSceneAsync("Student");
			SceneManager.UnloadSceneAsync("Clinic");
			Debug.Log("Remove clinic");
		} else {
			Debug.Log("Not level 1");
		}

		SceneManager.UnloadSceneAsync("Level");


	}

	public static void loadLevel() {
		SceneManager.LoadScene("Level", LoadSceneMode.Additive);
		SceneManager.UnloadSceneAsync("Level Transition Scene");
		
	}

	public static Texture2D getXray() {

		return xrays[level - 1];
	}

	public static Vector2 getCos() {
		return cos[level - 1];
	}


	

    
}
