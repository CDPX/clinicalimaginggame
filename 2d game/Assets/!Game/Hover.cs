﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hover : MonoBehaviour {

	public Texture2D crossMouse;
	public Texture2D moveMouse;
	public CursorMode curMode = CursorMode.ForceSoftware;
	public int z = 0;


	// Use this for initialization
	void Start () {

		crossMouse = GameObject.Find("Cross Mouse").GetComponent<SpriteRenderer>().sprite.texture;
		moveMouse = GameObject.Find("Move Mouse").GetComponent<SpriteRenderer>().sprite.texture;
		z = 3;

		float x = 16;
		float y = 16;
		Vector2 hotSpot = new Vector2(x,y);
		Cursor.SetCursor(moveMouse,hotSpot,curMode);

		print(crossMouse);
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("space")) {
			float x = 16;
			float y = 16;
			Vector2 hotSpot = new Vector2(x,y);
			Cursor.SetCursor(crossMouse,hotSpot,curMode);


		}

		


		
	}

	
}
