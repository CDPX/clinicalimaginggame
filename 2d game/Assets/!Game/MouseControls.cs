﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
//using UnityEngine.Animator;




public class MouseControls : MonoBehaviour {

	public static bool shootMouse;

	public static bool selectingSpot;

	public static bool canMove;
	public bool test;
	public static bool reset;

	public int cameraCurrentZoom = 5;
	public int cameraZoomMax = 100;
	public int cameraZoomMin = 1;

	public float mouseSensitivity = 0.05f;
 	private Vector3 lastPosition;

 	public GameObject weapon;

 	GameObject xray;

 	Camera cam;

 	Texture2D crossMouse;
	Texture2D moveMouse;

	Sprite crossMousePic;
	Sprite moveMousePic;


	public Texture2D handNormal;
	public Texture2D handClosed;

	public Sprite handNormalPic;
	

	public CursorMode curMode = CursorMode.ForceSoftware;

	public Animator animator;

	// Use this for initialization
	void Start () {

		canMove = true;

		cam = GameObject.Find("Minmap").GetComponent<Camera>();
		cam.orthographicSize = cameraCurrentZoom;
		
		xray = GameObject.Find("Xray");

		crossMousePic = GameObject.Find("Cross Mouse").GetComponent<SpriteRenderer>().sprite;
		moveMousePic = GameObject.Find("Move Mouse").GetComponent<SpriteRenderer>().sprite;

		crossMouse = GameObject.Find("Cross Mouse").GetComponent<SpriteRenderer>().sprite.texture;
		moveMouse = GameObject.Find("Move Mouse").GetComponent<SpriteRenderer>().sprite.texture;		

		float x = 16;
		float y = 16;
		Vector2 hotSpot = new Vector2(x,y);
		Cursor.SetCursor(handNormal,hotSpot,curMode);

		shootMouse = false;
		
	}
	
	// Update is called once per frame
	void Update () {

		if (reset) {
			resetScene();
		}

		Ray ray = cam.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		Physics.Raycast(ray, out hit);
		//Vector3 guess = hit.point;
		//print(shootMouse);

		if (shootMouse) {

			if (hit.collider == null) {
				selectingSpot = false;
			} else if (hit.collider.gameObject.name == "Xray") {
				selectingSpot = true;
			} else {
				selectingSpot = false;
			}

		} else {
			selectingSpot = false;
		}

		


		if (canMove) {
		     if (Input.GetAxis("Mouse ScrollWheel") < 0) {
		         if (cameraCurrentZoom < cameraZoomMax) {
		             cameraCurrentZoom += 1;
		             cam.orthographicSize = Mathf.Max(cam.orthographicSize + 1);
		         } 
		     }

		    if (Input.GetAxis("Mouse ScrollWheel") > 0) {
		         if (cameraCurrentZoom > cameraZoomMin) {
		             cameraCurrentZoom -= 1;
		             cam.orthographicSize = Mathf.Min(cam.orthographicSize - 1);
		         }   
		    }


		     if (Input.GetMouseButtonDown(0)) {
		         lastPosition = Input.mousePosition;
		     }
		 
		     if ((Input.GetMouseButton(0)) && (!selectingSpot)) {
		        Vector3 delta = lastPosition - Input.mousePosition;
		         transform.Translate(delta.x * mouseSensitivity, delta.y * mouseSensitivity, 0);
		         lastPosition = Input.mousePosition;

		        float x = 16;
				float y = 16;
				Vector2 hotSpot = new Vector2(x,y);
				Cursor.SetCursor(handClosed,hotSpot,curMode);
		     } else if (!shootMouse)  {
		     	float x = 16;
				float y = 16;
				Vector2 hotSpot = new Vector2(x,y);
				Cursor.SetCursor(handNormal,hotSpot,curMode);

		     }


		     if (Input.GetKeyDown("space")) {
		     	changeWeapon();
			}

		} else {

			float x = 16;
			float y = 16;
			Vector2 hotSpot = new Vector2(x,y);
			Cursor.SetCursor(handNormal,hotSpot,curMode);
		}



     }



     public void changeWeapon() {
     	float x = 16;
		float y = 16;
		Vector2 hotSpot = new Vector2(x,y);

		print(shootMouse);

     	if (!shootMouse) {
     		Cursor.SetCursor(crossMouse,hotSpot,curMode);
     		
     		shootMouse = true;
     		animator.Play("YTN No");

     	} else {
     		Cursor.SetCursor(moveMouse,hotSpot,curMode);

     		shootMouse = false;
     		animator.Play("YTN Yes");

     	}     	

     }

     public void resetScene() {

     	float x = 16;
		float y = 16;
		Vector2 hotSpot = new Vector2(x,y);
     	Cursor.SetCursor(moveMouse,hotSpot,curMode);
 		weapon.GetComponent<Image>().sprite = handNormalPic;
 		weapon.transform.localPosition = new Vector3(-13.6f,4.5f,0f);
 		shootMouse = false;

 		reset = false;

     }
}
 
