﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserControls : MonoBehaviour {

	public int cameraCurrentZoom = 50;
	public int cameraZoomMax = 100;
	public int cameraZoomMin = 1;

	public float mouseSensitivity = 0.05f;
 	private Vector3 lastPosition;

 	GameObject xray;

 	Camera cam;

 	public Texture2D crossMouse;
	public Texture2D moveMouse;
	public CursorMode curMode = CursorMode.ForceSoftware;

	// Use this for initialization
	void Start () {
		cam = GameObject.Find("Minmap").GetComponent<Camera>();
		cam.orthographicSize = cameraCurrentZoom;
		
		xray = GameObject.Find("Xray");

		crossMouse = GameObject.Find("Cross Mouse").GetComponent<SpriteRenderer>().sprite.texture;
		moveMouse = GameObject.Find("Move Mouse").GetComponent<SpriteRenderer>().sprite.texture;
		

		float x = 16;
		float y = 16;
		Vector2 hotSpot = new Vector2(x,y);
		Cursor.SetCursor(moveMouse,hotSpot,curMode);
		
	}
	
	// Update is called once per frame
	void Update () {
	     if (Input.GetAxis("Mouse ScrollWheel") < 0) {
	         if (cameraCurrentZoom < cameraZoomMax) {
	             cameraCurrentZoom += 1;
	             cam.orthographicSize = Mathf.Max(cam.orthographicSize + 1);
	         } 
	     }

	    if (Input.GetAxis("Mouse ScrollWheel") > 0) {
	         if (cameraCurrentZoom > cameraZoomMin) {
	             cameraCurrentZoom -= 1;
	             cam.orthographicSize = Mathf.Min(cam.orthographicSize - 1);
	         }   
	    }


	     if (Input.GetMouseButtonDown(0))
	     {
	         lastPosition = Input.mousePosition;
	     }
	 
	     if (Input.GetMouseButton(0))
	     {
	        Vector3 delta = Input.mousePosition - lastPosition;
	         transform.Translate(delta.x * mouseSensitivity, delta.y * mouseSensitivity, 0);
	         lastPosition = Input.mousePosition;
	     }


	     if (Input.GetKeyDown("space")) {
			float x = 16;
			float y = 16;
			Vector2 hotSpot = new Vector2(x,y);
			Cursor.SetCursor(crossMouse,hotSpot,curMode);


		}


     }
}
 
