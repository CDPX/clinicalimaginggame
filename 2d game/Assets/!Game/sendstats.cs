﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//sendstats.php file is used with this
public class sendstats : MonoBehaviour {

	//right now these values are used --> manually insert in unity
	public int image;
	public int level;
	public int completed;
	public int confidence;
	public int truepos;
	public int trueneg;
	public int falsepos;
	public int falseneg;




	//the url/path unity sends data to 
	public static string sendurl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/sendstats.php";
	// Use this for initialization


	//add time into this, couldnt figure out conversion stuff
	public static void addStats(int Imageid, int level, int completed, int confidence, 
							int truep, int truen, int falsep, int falsen, float timetaken){

		//Form enables you to send data to php file
		WWWForm statsForm = new WWWForm();


		statsForm.AddField("imagePost", Imageid);
		statsForm.AddField("levelPost", level);
		statsForm.AddField("completedPost", completed);
		//need to add this in 
		//statsForm.AddField("timePost",);
		statsForm.AddField("confidencePost", confidence);
		statsForm.AddField("tpPost", truep);
		statsForm.AddField("tnPost", truen);
		statsForm.AddField("fpPost", falsep);
		statsForm.AddField("fnPost", falsen);
		statsForm.AddField("timetaken", timetaken.ToString());

		//creates the connection and sends all the data 
		WWW statsWWW = new WWW(sendurl, statsForm);


	}
}
