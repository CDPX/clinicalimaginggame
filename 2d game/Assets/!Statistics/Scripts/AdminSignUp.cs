﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class AdminSignUp : MonoBehaviour {
    
    public GameObject username;
    public GameObject email;
    public GameObject password;
    public GameObject confirmPassword;
    public GameObject reasons;
    public Toggle toggle;
    private string Username;
    private string Email;
    private string Password;
    private string ConfirmPassword;
    private string Reasons;
    private string form;
    private bool EmailValid = false;

    public void signUpButton()
    {
        if (Username != "" && Email != "" && Password != "" && ConfirmPassword != "" && Reasons != "" && toggle.isOn)
        {
            print("Registration Successful");
        }
        else if (Username == "" && Email == "" && Password == "" && ConfirmPassword == "" && Reasons == "" && !toggle.isOn)
        {
            print("Please fill up all the required details, read and accept our T&C.");
        }
        else if (!toggle.isOn)
        {
            print("Please read and accept our T&C.");
        }
        else
        {
            print("Please fill up all the required details.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*
         * TODO: VERIFY EMAIL
         *       CHECK WHETHER PASSWORD MATCHES
         *       FIX THE UPLOAD STUFF
         */

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (username.GetComponent<InputField>().isFocused)
            {
                email.GetComponent<InputField>().Select();
            }
            if (email.GetComponent<InputField>().isFocused)
            {
                password.GetComponent<InputField>().Select();
            }
            if (password.GetComponent<InputField>().isFocused)
            {
                confirmPassword.GetComponent<InputField>().Select();
            }
            if (confirmPassword.GetComponent<InputField>().isFocused)
            {
                reasons.GetComponent<InputField>().Select();
            }
        }

        Username = username.GetComponent<InputField>().text;
        Email = email.GetComponent<InputField>().text;
        Password = password.GetComponent<InputField>().text;
        ConfirmPassword = confirmPassword.GetComponent<InputField>().text;
        Reasons = reasons.GetComponent<InputField>().text;
    }
}
