﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Experimental.UIElements;



[System.Serializable]
public class MyLvEvent : UnityEvent<int>{}

public class ChooseLv : MonoBehaviour
{

	public MyLvEvent lvEvent;
	Dropdown dropper;
	string lvUrls = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/getLevels.php";
	string[] Levels;
	int lvNo;
	// Use this for initialization
	IEnumerator Start ()
	{
		
		//Initializing variables
		if (lvEvent == null)
		{
			lvEvent = new MyLvEvent();
		}

		foreach (Transform child in transform.parent)
		{
//			Export ContainsExport = child.;
			
			// Only adds listeners that are not itself and do contain the export scritp
			if (child.name != this.name /*&& ContainsExport != null*/)
			{
				Debug.Log(child.name);
//				lvEvent.AddListener(child.GetComponent<Export>().changeLevel);
			}

		}

		dropper = this.GetComponent<Dropdown>();

		//Connecting to DB
		WWW LvLink = new WWW(lvUrls);

		yield return LvLink;
		
		//Cleaning the range before inserting it in (Getting rid off Null values)
		Levels = (LvLink.text.Split(';'));
		Levels = Levels.Take(Levels.Length - 1).ToArray();
		
		//clearing the drop down menu and rest
		dropper.ClearOptions();
		dropper.AddOptions(Levels.ToList());
		
		dropper.onValueChanged.AddListener(delegate
		{
			changeLv();
		});
	}	
	
	// Update is called once per frame
	void Update () {
//		dropper.onValueChanged.AddListener();
//		Debug.Log("Current Value: "  + dropper.captionText.text);
	}

	void changeLv()
	{
		if(Int32.TryParse(dropper.captionText.text,out lvNo))
		{
			Debug.Log("changed the level by dropdown");
			GameMan.level = lvNo;
		}
//		lvEvent.Invoke(newLv);
	}

	IEnumerator changeGameMan()
	{
		yield return new WaitUntil(() => dropper.options.Count > 1);
		if(Int32.TryParse(dropper.captionText.text,out lvNo))
		{
			GameMan.level = lvNo;
		}
	}
}
