using UnityEngine;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DBConn
{
    string getStatUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/GraphMaker.php";
    string confusionUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/confusion.php";
    string getUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/getstats.php";
    string getRecordUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/RecordTime.php";
    public static string ConfusionItem = "T_Pos:3|T_Neg:2|F_Pos:4|F_Neg:1;", StatItem,WorldRecordItem;
    public static string[] GraphItems;
    int lvNo = GameMan.level;
    /* Contains weather a connection has been successful or not
* [0] -> Confusion Connection Success
* [1] -> Graph Maker Connection (makeStats) Success
* [2] -> Dashboard Connection Success
* [3] -> World Record Connection Success
*/
    public static bool[] ConnReady = new bool[4] {false, false, false, false};
    // Use this for initialization
    [RuntimeInitializeOnLoadMethod]
    static void OnRuntimeMethodLoad() {
		
        //nextLevel();

    }

    public void setConfusion(string newConfusion)
    {
        ConfusionItem = newConfusion;       
    }



    /**
* Getter Functions
*/
	
    /*
     * Confusion Information
     */
    public static string getConfusionInfo()
    {
        if (ConnReady[0])
        {
            return ConfusionItem;
        }
        else
        {
            return null;
        }
    }

    public static string[] getGraphMakerInfo()
    {
        if (ConnReady[1])
        {
            return GraphItems;
        }
        else
        {
            return null;
        }
    }
	
    /*
     * Dashboard Information
     */
    public static string getDashInfo()
    {
        if (/*statReady*/ConnReady[2])
        {
            return StatItem;
        }
        else
        {
            return null;
        }
    }
	
    /*
     * World Record Information
     */
    public static string getRecordInfo()
    {
        if (ConnReady[3])
        {
            return WorldRecordItem;
        }
        else
        {
            return null;
        }
    }
    
    /*
 * Getting specific dataValues from the each row
 */
    public static string getDataValues(string data, string index,string splitter)
    {
        //Will start reading from that point
        string value = data.Substring(data.IndexOf(index) + index.Length);

        if (value.Contains(splitter))
        {
            //Will remove everything after the "|" INCLUDING IT!
            value = value.Remove(value.IndexOf(splitter));
        }

        if (value.EndsWith(";"))
        {
            value = value.Remove(value.Length - 1);
        }

//		print("The Value extracted["+index+"]: "+ value);

        return value;
    }
}