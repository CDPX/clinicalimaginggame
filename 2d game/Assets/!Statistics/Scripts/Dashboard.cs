﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.UI;

/**
 * @Author: Juan Manuel Cutrera
 */

public class Dashboard : MonoBehaviour
{
	Text Title;

	private string number;
//	public Export DBConnPrefab;
//	string getUrl = "http://localhost:8080/Clinical_images/getstats.php";
	
	// Use this for initialization
	IEnumerator Start ()
	{
		
		//Initialising the Variables
		Title = this.GetComponent<Text>();
//		while (DBConn.StatItem == null)
//		{
//			Debug.Log("Still loading...");
//		}

//		string test = DBConn.StatItem;
//		print("test:" + test);
		
//		Export  newExport = Instantiate(DBConnPrefab) as Export;

		
		//Wait for the export to connect to the Database.
		yield return new WaitUntil(() => DBConn.getDashInfo() != null);
		
		number = DBConn.getDashInfo();
		
		Title.text = Title.text.Replace("<LvNo>", number);

	}

	private void Update()
	{
		if (number != null)
		{
			if (!DBConn.getDashInfo().Equals(number))
			{
				string oldnumber = number;
				//updating the number
				Debug.Log("updateding");
				number = DBConn.getDashInfo();

				Title.text = Title.text.Replace(oldnumber,number);
				print(Title.text);
			}
		}


	}

	//Getting specific dataValues from the each row
	public string getDataValues(string data, string index)
	{
		//Will start reading from that point
		string value = data.Substring(data.IndexOf(index) + index.Length);

		if (value.Contains("|"))
		{
			//Will remove everything after the "|" INCLUDING IT!
			value = value.Remove(value.IndexOf("|"));
		}
		
		print("The Value extracted["+index+"]: "+ value);

		return value;
	}
}
