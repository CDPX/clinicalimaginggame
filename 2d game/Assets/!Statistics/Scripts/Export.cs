﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor.IMGUI.Controls;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;


/*
 * So Graph maker doesn't need to wait for others to complete
 */
//
public class Export : MonoBehaviour
{

	string getStatUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/GraphMaker.php";
	string confusionUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/confusion.php";
	string getUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/getstats.php";
	string getRecordUrl = "http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com/RecordTime.php";
	private static string ConfusionItem, StatItem = "",WorldRecordItem, File;
	string[] Items;
	int lvNo = GameMan.level;

	private bool atConfusion = false, DbConn = false, started = false;
	
	/* Contains weather a connection has been successful or not
	 * [0] -> Confusion Connection Success
	 * [1] -> Graph Maker Connection (makeStats) Success
	 * [2] -> Dashboard Connection Success
	 * [3] -> World Record Connection Success
	 */
	private bool[] ConnReady = new bool[4] {false, false, false, false};

	// Use this for initialization
	void Start ()
	{
		
		started = true;
		
		File = "Mi Dulce Panquecito con Dulce de leche\n\n\n\n" +
		       "Te amo";
//		 //Manager.level
		/*
		 * Call the new manager script
		 * Change level no. to Managers level number
		 * int lvNo = Manager.level;
		 */
		couroutineManager();
	}
	
	// Update is called once per frame
	void Update () {
		if (lvNo != GameMan.level)
		{
			Debug.Log("changing the game mane lv in Export");
			lvNo = GameMan.level;
			//Reload them
			couroutineManager(false);
			couroutineManager();
		}
	}

	public void changeLevel(int i)
	{
		/*
		 * Manager.level = i
		 * Manager.changeLevel();
		 */
		Debug.Log("chaned the level: " + i);
		lvNo = i;
		Start();//re-load

	}

	public void pressme()
	{

		Debug.Log("export button Pressed!s");
		for (int i = 0; i < ConnReady.Length; i++)
		{
			if (i == 2)
			{
				continue;
			}

			bool Connection = ConnReady[i];
			if (!Connection)
			{
				print("PLease wait for Database to be downloaded\n" +
				      "Try again later");
				return;
			}
		}
		Debug.Log("Button pressed");
		File = makecsvFile();
		System.IO.File.WriteAllText("Statistics LV:"+lvNo+".csv",File);
	}

	
	/**
	 * The Data base connection fucntions
	 */
	
	/*
	 * Confusion Connection
	 */
	IEnumerator makeConfusion(int LvNo)
	{
//		atConfusion = true;
		WWWForm Form = new WWWForm();
		Form.AddField("image_id", LvNo);
		WWW statLink = new WWW(confusionUrl, Form);

		yield return statLink;

		//Updating Variables
		ConfusionItem = statLink.text;
		//Adding to the File
		File = ConfusionItem;
		atConfusion = false;
		ConnReady[0] = true;
		DBConn.ConfusionItem = ConfusionItem;
		DBConn.ConnReady[0] = true;
	}

	/*
	 * GraphMaker Connection
	 */
	IEnumerator makeStats(int LvNo)
	{
//		yield return new WaitWhile(() => atConfusion);
		WWWForm Form = new WWWForm();
		Form.AddField("image_id", LvNo);
		WWW statLink = new WWW(getStatUrl, Form);
		yield return statLink;

		Items = statLink.text.Split(';');
		Items = Items.Take(Items.Length - 1).ToArray();
		
		//Placing it onto File
		foreach (string Item in Items)
		{
			File = File + Item;
		}
		File = File + "\n";
		ConnReady[1] = true;
		DbConn = true;
		DBConn.ConnReady[1] = true;
		DBConn.GraphItems = Items;

	}

	/*
	 * Dashboard Connection
	 */
	IEnumerator makeDashboard(int LvNo)
	{
		WWWForm Form = new WWWForm();
		Form.AddField("image_id", LvNo);
		WWW statLink = new WWW(getUrl, Form);

		yield return statLink;

		//Update Variables
		StatItem = statLink.text;
		ConnReady[2] = true;
		DBConn.ConnReady[2] = true;
		DBConn.StatItem = StatItem;
	}
	
	/*
	 * World Record Connection
	 */
	IEnumerator makeRecord(int LvNo)
	{
		WWWForm Form = new WWWForm();
		Form.AddField("image_id", LvNo);
		WWW statLink = new WWW(getRecordUrl, Form);

		yield return statLink;

		//Update Variables
		WorldRecordItem = statLink.text;
		File = File + statLink.text;
		ConnReady[3] = true;
		DBConn.ConnReady[3] = true;
		DBConn.WorldRecordItem = WorldRecordItem;
	}

	/**
	 * Getter Functions
	 */
	
	/*
	 * Confusion Information
	 */
	public string getConfusionInfo()
	{
		if (ConnReady[0])
		{
			return ConfusionItem;
		}
		else
		{
			return null;
		}
	}

	public string[] getGraphMakerInfo()
	{
		if (ConnReady[1])
		{
			return Items;
		}
		else
		{
			return null;
		}
	}
	
	/*
	 * Dashboard Information
	 */
	public string getDashInfo()
	{
		if (/*statReady*/ConnReady[2])
		{
			return StatItem;
		}
		else
		{
			return null;
		}
	}
	
	/*
	 * World Record Information
	 */
	public string getRecordInfo()
	{
		if (ConnReady[3])
		{
			return WorldRecordItem;
		}
		else
		{
			return null;
		}
	}

	public bool getDashReady()
	{
		return ConnReady[2];
	}


	/** Maker Functions
	 * TODO: OPTIMISE 
	 */
	string makecsvFile()
	{
		string newFile ="";
		Debug.Log(File);
		
		/**
		 * Graph Maker stats
		 */
		//making the first Row (Titles)
		for(int index = 0 ; index < Items.Length; index++)
		{
			string row = Items[index];

			if (index == 0)
			{
				newFile += itemToCSV(row, true);
			}
			else
			{
				newFile += itemToCSV(row);
			}
		}
		newFile = newFile + "\n";
		/**
		 * Confusion matrix and User record
		 */
		newFile += itemToCSV(ConfusionItem, true);
		newFile += "\n";
		newFile += itemToCSV(WorldRecordItem,true);
		
		/**
		 * End of graph make
		 */
		return newFile;
	}

	string itemToCSV(string Item, bool keepTitle = false)
	{
		Debug.Log("how it was before transformation: " + Item);
		string CSVReady = "";
		string values = "";
			string[] Columns = Item.Split('|');

			for(int index = 0; index < Columns.Length; index++)
			{
				string ColVal = Columns[index];
				//The Title

					ColVal = ColVal.Remove(ColVal.IndexOf(':'));

					values += getDataValues(Item, ColVal + ":", "|");
				//Add Values
				CSVReady = CSVReady + ColVal;
				
				//Checking if its the lastValue
				if ( index + 1 == Columns.Length)
				{
//					CSVReady = CSVReady + ColVal;
				}
				else
				{
					values += ",";
					CSVReady += ",";
				}
				
			}
		CSVReady += "\n";
		/* New Row needs to be added*/
		if (keepTitle)
		{
			CSVReady += values + "\n";
		}
		else
		{
			CSVReady = values + "\n";
		}
			Debug.Log(CSVReady);

		return CSVReady;
	}

	/*
	 * Getting specific dataValues from the each row
	 */
	public string getDataValues(string data, string index,string splitter)
	{
		//Will start reading from that point
		string value = data.Substring(data.IndexOf(index) + index.Length);

		if (value.Contains(splitter))
		{
			//Will remove everything after the "|" INCLUDING IT!
			value = value.Remove(value.IndexOf(splitter));
		}

		if (value.EndsWith(";"))
		{
			value = value.Remove(value.Length - 1);
		}

//		print("The Value extracted["+index+"]: "+ value);

		return value;
	}

	/**
	 * Manages the couroutines (connections to DB)
	 * @variables Start
	 * + Start: begin the couroutines
	 * - Start: stop all couroutines
	 */
	void couroutineManager(bool Start = true)
	{
		if (Start)
		{
			StopAllCoroutines();	
			StartCoroutine(makeConfusion(lvNo));
			StartCoroutine(makeStats(lvNo));
			StartCoroutine(makeDashboard(lvNo));
			StartCoroutine(makeRecord(lvNo));
		}
		else
		{
			StopAllCoroutines();
		}

	}

}
