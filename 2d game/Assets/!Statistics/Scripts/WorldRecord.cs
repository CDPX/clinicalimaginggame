﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class WorldRecord : MonoBehaviour
{
	Text WorldRecordTitle;

	private string RecordHolder;
//	public Export DBConnPrefab;
	
	// Use this for initialization
	IEnumerator Start () {
		
		//Initialising the Variables
		WorldRecordTitle = this.GetComponent<Text>();
//		Export  newExport = Instantiate(DBConnPrefab) as Export;
		
//		newExport.transform.SetParent(transform);
		
		//Wait for the export to connect to the Database.
		yield return new WaitUntil(() => DBConn.getRecordInfo() != null);
		
		 RecordHolder = DBConn.getRecordInfo();

		//Updte the World Record Title
		updateRecord();
	}
	
	// Update is called once per frame
	void Update () {
		if (RecordHolder != null)
		{
			if (!DBConn.getRecordInfo().Equals(RecordHolder))
			{
				Debug.Log("Changing record Holder");
				RecordHolder = DBConn.getRecordInfo();
				updateRecord();
			}
		}
	}

	void updateRecord()
	{
		WorldRecordTitle.text = DBConn.getDataValues(RecordHolder, "Player: ", "|") + "\n";
		WorldRecordTitle.text += DBConn.getDataValues(RecordHolder, "World Record: ", "|") + "\n";
		WorldRecordTitle.text += "World Record\n(hh:mm:ss)";
	}
}
