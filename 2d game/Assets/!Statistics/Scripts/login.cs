﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class login : MonoBehaviour {

    public Text username;
    public Text password;
    string loginurl = "http://localhost/player.php";
    string usrstr;
    string pwdstr;
    public Button Button;

    // Use this for initialization
    void Start () {
        Button btn1 = Button.GetComponent<Button>();

        btn1.onClick.AddListener(runrun);
	}

    private void runrun()
    {
        usrstr = username.text;
        pwdstr = password.text;
        logintodb(usrstr, pwdstr);
    }
	
	// Update is called once per frame
	void Update () {
        //if (Input.GetKeyDown(KeyCode.L)) logintodb(usernamestr, passwordstr);
        usrstr = username.text;
        pwdstr = password.text;
    }

    IEnumerator logintodb(string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);

        WWW www = new WWW(loginurl, form);
        yield return www;
    }
}
