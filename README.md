# **README** 

## For future Developers  

[Play the game](http://ec2-13-236-68-115.ap-southeast-2.compute.amazonaws.com"Click here to start the game")  
*You will notice the bugs from playing the game.*   

[Accessing the Game (backend)](https://docs.google.com/document/d/1QToxpYoEVKxEVY4ozY7_VjUIPkcFrS99B0cI7I6tf0E/edit?usp=sharing)  

## **In terms of accessing the code: The game is in the stats branch and you need to use git lfs.**  

**The code is split up into four sections:**

* Game - 2D and 3D 
* Classroom
* Statistics
* Menu  

Where to find information (*Hint: look into the wikis or the last individual final report for the info*):  
* Rhys was the main developer so his individual reflections will have all the information about challenges he faced  
* Juan worked on stats  
* Jin worked on the classroom  
* To find user stories that is in our final report  

### Scenes: ###
## Game: ##
* Level - A single level of the overall game
* Level Transition Scene - The animation of the level changing

## Classroom  
The idea is you have 2 portals, one as an admin and one as a student. When the admin logs in all they should see is the "create classroom" button. Which allows them to specify the difficulty level and the number of images they want. Then they are shown the images depending on the difficult. These images should be customisable, so maybe display the images they can choose from. After this, they are given a "unique code" which is stored in the database. 

For the users, the code is used to access the classroom created by the admin above. Then the game starts with those specific images. 

### **Teacher:**
* Difficulty - The teacher chooses the difficulty and the number of levels - TODO: Custom difficulties
* Finalise image - A grid of images are loaded - TODO: Changing images and uploading custom images

* Classroom random code - A random code is generated and the classroom is now in the database

### **Student:**
* Student - The student enters in a given code to load the image

## Stats   
GIT LFS is used for the stats branch so if you want to access this look into it :)  *(See below for more info on LFS)*
Dashboard - Shows statistics about different levels  
This should show the following information.  

* time taken to complete levels  
* screen resolutions  
* FP, TP, FN, TN  
* expertise of players and performance 

## Menu 
Clinic - 3d scene where the user registers, logins and can navigate to other areas

TODO: Highlight items when hovered over


### Future Scenes: ###
* Loading screens
* Teachers have menu to see all past classrooms

**Issues to solve/Things to do/Tips:**

* Smaller images in the database for shorter load time in the finalise image scene  
* Statistics page is not compatible with WebGL because unity editor functions were utilised! This needs to be fixed so it can be used on the web.  
* The upload functionality was not integrated to the game  
* Not all the images are in the database because we could not convert the .IMG files from the japanese one (this is a big TODO)     
* Only the admin should be able to access the classroom  
* Various bug fixes to be done with the classroom (one of the team members decided to rewrite the entire code again for this portion the night before and pretty set us back by 4 weeks, hence the following tasks)  
   1. only the admin should be able to make a classroom
   2. only the user should be able to access a classroom  
   3. only the user should be able to access a classroom  
   4. The grid does not alow you to choose the images successfully   
   5. The current image generation is not very good so you'll have to improve it.  
* Choosing the weapon doesn't work, need to add weapons and allow the user to choose it.  
* Just make sure everything is compatible with WebGL as you go.  
* link scenes straight away as you finish them for integration testing  
* Don't change everything last minute because it won't work, unless you 100% know what you are doing!


***
 
# LFS  _Large Storage File_
Below is a mini tutorial showing how to use LFS on your branches (Installing it & Using it).
*About LFS*
It’s a way to make our GitBucket lighter without deleting any branches or files. (Uses pointers :wink:)
https://youtu.be/9gaTargV5BY

*Instaling*
> On Mac:  `brew install git-lfs`
> Zip File: _https://git-lfs.github.com/_
> Having *SourceTree* (Its included)

*Using it*

> Ensure its installed:
> `git lfs install`
> `git lfs track <pattern>` <– Makes note of the pattern and places it on `.gitattribute`

> `git add <pattern files>` <–Normal add

> `git commit -m 'Added the LFS Files'`

> `git push <remote> <branch>`

*********
MORE INFO:
- https://www.atlassian.com/git/tutorials/git-lfs#installing-git-lfs
- https://github.com/git-lfs/git-lfs/wiki/Tutorial