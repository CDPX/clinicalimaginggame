﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TMPro.EditorUtilities;
using UnityEditor.Experimental.UIElements.GraphView;
using UnityEngine;
using UnityEngine.UI;


public class BarChart : MonoBehaviour
{
	public Camera camera;
	//Will be available for user to interact with
	public Bar barPrefab;
	string geturl = "http://localhost:8080/Clinical_images/confusion.php";

	string Item;
	/* Default Values
	   - Restricted bar counter to 4
	 */
	public int[] inputValues = new int[4] {0,0,0,0}; 
	string[] labels = {"TP","FN","FP","TN"};
	Color[] colors = {Color.white, new Color32(0x08,0x91,0x08,0xFF), Color.red, new Color32(0x00,0x92,0xB2,0xFF)};
	
	List<Bar> bars = new List<Bar>();

//	Confusion statistics = new Confusion();	//problem is in confusion
	
	
	float chartHeight;
	// Use this for initialization
	IEnumerator Start ()
	{
		camera = GameObject.Find("Main Camera").GetComponent<Camera>();
		

		WWW statLink = new WWW(geturl);
		//Wait Until imgLink is Done downloading
		yield return statLink;

		Item = statLink.text;
		
		chartHeight = Screen.height / 2;
		int[] values = {10, 25, 50, 40};

		print("The Item: " +Item);
		
		DisplayGraph(getValues(Item));
		
	}

//	void Update()
//	{
//		//Cretes a pointer from your mouse
//		Ray Ray = camera.ScreenPointToRay(Input.mousePosition);
//		RaycastHit hit;
//		Physics.Raycast(Ray, out hit);
//
//		print(hit.point);
//		if (hit.collider != null)
//		{
//			print(hit.collider.gameObject.name);
//		}
//	}

	void DisplayGraph(int[] vals)
	{

		
	
		
		
		//stats.getDataValues(stats.getItem(), "T_Pos:");
		
		
		print(gameObject.GetComponent<Confusion>().getItem());
//		print("In display Graph: "+ statistics.Item);
		
		int maxValue = vals.Max();
		for (int i = 0; i < vals.Length; i++)
		{
			//Makes an new Instance of 'bar Prefab"
			Bar newBar = Instantiate(barPrefab) as Bar;
			
			//Add that new instance as a child to the Parent (barChart)
			newBar.transform.SetParent(transform);
			newBar.name = labels[i];
			
			//size Bar
			RectTransform rt = newBar.bar.GetComponent<RectTransform>();
			
			//To adjust the columns to the grid (the 0.95f is to contain everything on the graph)
			float normalizedValue = ((float)vals[i] / (float)maxValue) * 0.95f;
			rt.sizeDelta = new Vector2(rt.sizeDelta.x , normalizedValue * chartHeight);

			//Rotate the bars so they go downwards
			if (i > 1)
			{
				rt.Rotate(new Vector3(180,0,0));
				rt.transform.localPosition = new Vector2(0,50);
			}
			
			//Keeps on repeating the same colours
			newBar.bar.color = colors[i % colors.Length];
			
			/*Adding Labels to bars*/
			if (labels.Length <= i)
			{
				newBar.Label.text = "UNDEFINED";
			}
			else
			{
				//Adding the labels
				newBar.Label.text = labels[i];
				
				//Label changes if the value is equal to 0
				if (vals[i] == 0) newBar.Label.text = labels[i] + " | NO DATA";
			}
			
			/*Adding barValues text to bars*/
//			newBar.barValue.text = vals[i].ToString();
//			
//			//if height is too small, move label to top of bar
//			if (rt.sizeDelta.y < 30f)
//			{
//				newBar.barValue.rectTransform.pivot = new Vector2(0.5f, 0f);
//				//Position is based on the pivot point
//				newBar.barValue.rectTransform.anchoredPosition = Vector2.zero;
//			}
		}
	}

	int[] getValues( string item)
	{
		int[] extractedValues = new int[4];

		//Extract individually
		Int32.TryParse(getDataValues(item, "T_Pos:"), out extractedValues[0]);
		Int32.TryParse(getDataValues(item, "F_Neg:"), out extractedValues[1]);
		Int32.TryParse(getDataValues(item, "F_Pos:"), out extractedValues[2]);
		Int32.TryParse(getDataValues(item, "T_Neg:"), out extractedValues[3]);

		
		return extractedValues;
		
	}
	//Getting specific dataValues from the each row
	public string getDataValues(string data, string index)
	{
		//Will start reading from that point
		string value = data.Substring(data.IndexOf(index) + index.Length);

		if (value.Contains("|"))
		{
			//Will remove everything after the "|" INCLUDING IT!
			value = value.Remove(value.IndexOf("|"));
		}
		
		print("The Value extracted["+index+"]: "+ value);

		return value;
	}
}

 