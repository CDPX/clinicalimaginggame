﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Confusion : MonoBehaviour {

	//Whatever is public here then it'll display
	 public string Item;
	//url --> where the info is posted
	//CHANGE HERE!
	string geturl = "http://localhost:8080/Clinical_images/confusion.php";
	
	// Use this for getting stats
	IEnumerator Start(){


//		//to get Stats
//		WWWForm statsForm = new WWWForm();
//		//change number 1 to a variable which will store the stats
//		statsForm.AddField("stats_Post", 1);
//
//		WWW statLink = new WWW("http://localhost:8080/Clinical_images/confusion.php", statsForm);
		WWW statLink = new WWW(geturl);
		
		//Wait Until imgLink is Done downloading
		yield return statLink;

		Item = "HELLO";
		Item = statLink.text;
//		print("URL" +statLink.text);
//		print("The item confusion: " + getItem());
		
	}

	//Getting specific dataValues from the each row
	public string getDataValues(string data, string index)
	{
		//Will start reading from that point
		string value = data.Substring(data.IndexOf(index) + index.Length);

		if (value.Contains("|"))
		{
			//Will remove everything after the "|" INCLUDING IT!
			value = value.Remove(value.IndexOf("|"));
		}

		print("The Value extracted["+index+"]: "+ value);
		return value;
	}

	public string getItem()
	{
		return Item;
	}
}
