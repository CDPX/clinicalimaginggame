﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getstats : MonoBehaviour {

	
	//Constructor
	public getstats(bool choice = true)
	{
		print("Calling Constructor");
//		print(geturl);
		if (choice)
		{
			geturl = "http://localhost:8080/Clinical_images/confusion.php";
		}
		else
		{
			geturl = "http://localhost:8080/Clinical_images/getstats.php";
		}

		print(geturl);
	}

	

	//Whatever is public here then it'll display
	public string[] items;
	//url --> where the info is posted
	//CHANGE HERE!
	string geturl = "http://localhost:8080/Clinical_images/getstats.php";
	
	// Use this for getting stats
	IEnumerator Start() {



		//to get Stats
		WWWForm statsForm = new WWWForm();
		//change number 1 to a variable which will store the stats
		statsForm.AddField("stats_Post", 1);

		WWW statLink = new WWW(geturl, statsForm);
		
		//Wait Until imgLink is Done downloading
        yield return statLink;

		string statElements = statLink.text;
		
		//Splits the string by the deliminter ';'
		items = statElements.Split(';');
	}

	//Getting specific dataValues from the each row
	public string getDataValues(String data, String index)
	{
		//Will start reading from that point
		string value = data.Substring(data.IndexOf(index) + index.Length);

		if (value.Contains("|"))
		{
			//Will remove everything after the "|" INCLUDING IT!
			value = value.Remove(value.IndexOf("|"));
		}

		return value;
	}
}
